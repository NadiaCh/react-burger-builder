import React, { Component } from 'react';
import PropsTypes from 'prop-types';

import Aux from '../Aux/Aux';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component {s

    state = {
        isShowSideDrawer: false
    }

    sideDrawerClosedHandler = () => {
        this.setState({ isShowSideDrawer: false });
    }

    sideDrawerOpenedHandler = () => {
        this.setState((prevState) => (
            { isShowSideDrawer: !prevState.isShowSideDrawer }
        ));
    }

    render() {
        return (
            <Aux>
                <Toolbar sideDrawerOpened={this.sideDrawerOpenedHandler} />
                <SideDrawer
                    open={this.state.isShowSideDrawer}
                    closed={this.sideDrawerClosedHandler}
                />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

Layout.propTypes = {
    children: PropsTypes.element
}

export default Layout;