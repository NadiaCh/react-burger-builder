import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classes from './Modal.module.css';
import Aux from '../../../hoc/Aux/Aux';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {

    shouldComponentUpdate(newProps, nextState) {
        return newProps.isShow !== this.props.isShow || newProps.children !== this.props.children;
    }

    render() {
        return (<Aux>
            <Backdrop isShow={this.props.isShow} clicked={this.props.modalClosed}></Backdrop>
            <div
                className={classes.Modal}
                style={{
                    transform: this.props.isShow ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: this.props.isShow ? '1' : '0'
                }}
            >
                {this.props.children}
            </div>
        </Aux>);
    } 
}

Modal.propTypes = {
    isShow: PropTypes.bool.isRequired
}

export default Modal;