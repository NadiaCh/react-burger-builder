import React from 'react';
import PropsTypes from 'prop-types';

import classes from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const burger = (props) => {
    let transformedIngredients = Object.keys(props.ingredients)
        .map(igKey => [...Array(props.ingredients[igKey])].map((_, i) =>
            (
                <BurgerIngredient key={igKey + i} type={igKey} />
            ))
        )
        .reduce((arr, el) => arr.concat(el), []);
    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients!</p>;
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

burger.propTypes = {
    ingredients: PropsTypes.shape({
        salad: PropsTypes.number,
        bacon: PropsTypes.number,
        cheese: PropsTypes.number,
        meat: PropsTypes.number
    }).isRequired
};

export default burger;